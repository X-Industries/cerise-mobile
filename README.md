# Cerise Mobile

Mobile version of the Cerise media player. Based on Android's built-in video player.

This version was created with Kodular, which is proprietary. However, there are plans to migrate to a FOSS alternative.

## Installation

### Manual

1. Download the APK file from the [Releases](https://codeberg.org/X-Industries/cerise-mobile/releases) tab.
2. If you haven't already, enable installation from unknown sources in you settings.
3. Tap on the APK file you just downloaded.
4. Follow the installer

## Screenshots

<img src="http://x-industries.co.uk/wp-content/uploads/2022/06/Screenshot_2022-06-14-13-20-37.png" width="288px" height="512px" alt="Main Menu"> <img src="http://x-industries.co.uk/wp-content/uploads/2022/06/Screenshot_2022-06-14-13-23-50.png" width="288px" height="512px" alt="Portrait Mode">

<img src="http://x-industries.co.uk/wp-content/uploads/2022/06/Screenshot_2022-06-14-13-30-49.png" width="576px" height="324px" alt="Landscape Mode, showing controls">